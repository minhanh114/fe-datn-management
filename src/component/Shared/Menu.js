import React, { useState, useContext, useEffect } from 'react';
import {
    NavLink,
    Redirect
} from "react-router-dom";
import { AuthContext } from '../context/Auth'

function Menu() {
    const { user, jwt } = useContext(AuthContext);

    const [menu, setMenu] = useState([
        {
            item: "Khách hàng",
            permission: "Nhân Viên"
        },
        {
            item: "Phiếu giảm giá",
            permission: "Nhân Viên"
        },
        {
            item: "Sản phẩm",
            permission: "Nhân Viên"
        },
        {
            item: "Khuyến mãi",
            permission: "Nhân Viên"
        },
        {
            item: "Thể loại",
            permission: "Nhân Viên"
        },
        {
            item: "Đơn hàng",
            permission: "Nhân Viên"
        },
        {
            item: "Xác nhận đơn hàng",
            permission: "Nhân Viên"
        },
        {
            item: "Giao hàng",
            permission: "Nhân Viên"
        },
        {
            item: "Xác nhận giao hàng",
            permission: "Nhân Viên"
        },
        {
            item: "Đơn hàng hoàn thành",
            permission: "Nhân Viên"
        },
        {
            item: "Đơn hàng hủy",
            permission: "Nhân Viên"
        },
        {
            item: "Người dùng",
            permission: "Admin"
        },
        {
            item: "Quyền",
            permission: "Admin"
        }
        // "Category", ,
        // "Permission",
        // "User",
        // "Order",
        // "ConfirmOrder",
        // "Delivery",
        // "ConfirmDelivery",   
        // "CompletedOrder",
        // "CancelOrder"
    ])

    function convertCategortList(data) {
        const lowerCaseData = data.toLowerCase();

        if (lowerCaseData.localeCompare('khách hàng', 'vi') === 0) {
            data = 'Customer'
        } else if (lowerCaseData.localeCompare('phiếu giảm giá', 'vi') === 0) {
            data = 'Coupon'
        } else if (lowerCaseData.localeCompare('sản phẩm', 'vi') === 0) {
            data = 'Product'
        } else if (lowerCaseData.localeCompare('khuyến mãi', 'vi') === 0) {
            data = 'Sale'
        } else if (lowerCaseData.localeCompare('thể loại', 'vi') === 0) {
            data = 'Category'
        } else if (lowerCaseData.localeCompare('đơn hàng', 'vi') === 0) {
            data = 'Order'
        } else if (lowerCaseData.localeCompare('xác nhận đơn hàng', 'vi') === 0) {
            data = 'ConfirmOrder'
        } else if (lowerCaseData.localeCompare('giao hàng', 'vi') === 0) {
            data = 'Delivery'
        } else if (lowerCaseData.localeCompare('xác nhận giao hàng', 'vi') === 0) {
            data = 'ConfirmDelivery'
        } else if (lowerCaseData.localeCompare('đơn hàng hoàn thành', 'vi') === 0) {
            data = 'CompletedOrder'
        } else if (lowerCaseData.localeCompare('đơn hàng hủy', 'vi') === 0) {
            data = 'CancelOrder'
        } else if (lowerCaseData.localeCompare('người dùng', 'vi') === 0) {
            data = 'User'
        } else if (lowerCaseData.localeCompare('quyền', 'vi') === 0) {
            data = 'Permission'
        }
        return data
    }
    let { pathname } = window.location;
    return (
        <div>
            {
                jwt && user ?
                    (
                        <aside className="left-sidebar" data-sidebarbg="skin6">
                            <div className="scroll-sidebar" data-sidebarbg="skin6" style={{ overflowY: 'auto', maxHeight: '100vh' }}>
                                <nav className="sidebar-nav">
                                    <ul id="sidebarnav">
                                        <li className="list-divider"></li>
                                        <li className="nav-small-cap"><span className="hide-menu">Danh mục</span></li>
                                        <li className="sidebar-item"> <a className="sidebar-link has-arrow" href="#"
                                            aria-expanded="false"><i data-feather="grid" className="feather-icon"></i><span
                                                className="hide-menu">Bảng</span></a>
                                            <ul aria-expanded="false" className="collapse  first-level base-level-line">
                                                {
                                                    menu && menu.map((item, index) => (
                                                        (
                                                            <li className="sidebar-item active" key={index}>
                                                                {
                                                                    item.permission === user.id_permission.permission ?
                                                                        (
                                                                            <NavLink to={"/" + convertCategortList(item.item.toLowerCase())} className="sidebar-link">
                                                                                {item.item}
                                                                            </NavLink>
                                                                        ) :
                                                                        (
                                                                            <div></div>
                                                                        )
                                                                }
                                                            </li>
                                                        )
                                                    ))
                                                }
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </aside>
                    ) :
                    (
                        <Redirect to="/" />
                    )
            }
        </div>


    );
}

export default Menu;