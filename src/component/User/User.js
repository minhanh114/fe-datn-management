import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import { useAlert } from 'react-alert';


import userAPI from '../Api/userAPI';
import Pagination from '../Shared/Pagination'
import Search from '../Shared/Search'

function User(props) {
    const [filter, setFilter] = useState({
        page: '1',
        limit: '4',
        search: '',
        status: true
    })

    const [users, setUsers] = useState([])
    const [totalPage, setTotalPage] = useState()
    const alert = useAlert()



    useEffect(() => {
        const query = '?' + queryString.stringify(filter)

        const fetchAllData = async () => {
            const response = await userAPI.getAPI(query)
            setUsers(response.users)
            setTotalPage(response.totalPage)
        }
        fetchAllData()
    }, [filter])


    const onPageChange = (value) => {
        setFilter({
            ...filter,
            page: value
        })
    }

    const handlerSearch = (value) => {
        setFilter({
            ...filter,
            page: '1',
            search: value
        })
    }

    const handleDelete = async (value) => {
        const query = '?' + queryString.stringify({ id: value._id })

        const response = await userAPI.delete(query)

        if (response.msg === "Thanh Cong") {
            alert.success('Xóa thành công')
            setFilter({
                ...filter,
                status: !filter.status
            })
        }
    }


    return (
        <div className="page-wrapper">

            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Người dùng</h4>
                                <Search handlerSearch={handlerSearch} />

                                <Link to="/user/create" className="btn btn-primary my-3">Tạo mới</Link>

                                <div className="table-responsive">
                                    <table className="table table-striped table-bordered no-wrap">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Tên</th>
                                                <th>Email</th>
                                                <th>Quyền</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            {
                                                users && users.map((value, index) => (
                                                    <tr key={index}>
                                                        <td>{value._id}</td>
                                                        <td>{value.fullname}</td>
                                                        <td>{value.email}</td>
                                                        <td>{value.id_permission?.permission}</td>
                                                        <td>
                                                            <div className="d-flex">
                                                                <Link to={"user/update/" + value._id} className="btn btn-success mr-1">Cập nhật</Link>

                                                                <button type="button" style={{ cursor: 'pointer', color: 'white' }} onClick={() => handleDelete(value)} className="btn btn-danger" >Xóa</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                    </table>

                                </div>
                                <Pagination filter={filter} onPageChange={onPageChange} totalPage={totalPage} />
                            </div>

                        </div>
                    </div>
                </div>


            </div>
            <footer className="footer text-center text-muted">
                Đã đăng ký bản quyền bởi Dimples. Thiết kế và phát triển bởi <a
                    href="https://www.facebook.com/Minh Anh.9920/">Minh Anh</a>.
            </footer>
        </div>
    );
}

export default User;