import React, { useEffect, useState } from 'react';
import { useForm, Controller } from 'react-hook-form'
import AutoComplete from '@mui/material/Autocomplete';
import { Box, Grid, TextField } from "@mui/material";
import queryString from 'query-string';
import { useParams } from 'react-router';
import CouponAPI from '../Api/CouponAPI';
import userAPI from '../Api/userAPI';


const defaultValues = {
    code: '',
    count: '',
    promotion: '',
    describe: '',
    users: []
};

function UpdateCoupon(props) {

    const [showMessage, setShowMessage] = useState('')

    const { id } = useParams()
    const { control, setValue } = useForm();


    const [code, setCode] = useState('')
    const [count, setCount] = useState('')
    const [promotion, setPromotion] = useState('')
    const [describe, setDescribe] = useState('')
    const [selectedUsers, setSelectedUsers] = useState([]); // Track selected users
    const [customers, setCustomers] = useState([]);

    const [filter, setFilter] = useState({
        permission: '65389cb5d833104afc9959fe'
    })

    useEffect(() => {
        const query = '?' + queryString.stringify(filter)

        const fetchAllData = async () => {
            const response = await userAPI.getAPI(query)
            setCustomers(response.users)
        }
        fetchAllData()
    }, [filter])

    const { register, handleSubmit, formState: { errors }, reset } = useForm({ defaultValues });
    const onSubmit = async (data) => {

        const body = {
            code: code,
            count: count,
            promotion: promotion,
            describe: describe,
            users: selectedUsers.map(user => user._id)
        }

        const response = await CouponAPI.updateCoupon(id, body)

        setShowMessage(response.msg)

    };

    useEffect(() => {

        const fetchData = async () => {
            const response = await CouponAPI.getCoupon(id)
            setCode(response.code)
            setCount(response.count)
            setPromotion(response.promotion)
            setDescribe(response.describe)
            setSelectedUsers(response.infoUser);
        }

        fetchData()
    }, [])


    return (
        <div className="page-wrapper">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Cập nhật phiếu</h4>
                                {
                                    showMessage === "Bạn đã cập nhật thành công" ?
                                        (
                                            <div className="alert alert-success alert-dismissible fade show" role="alert">
                                                {showMessage}
                                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                        ) :
                                        (
                                            <p className="form-text text-danger">{showMessage}</p>
                                        )
                                }


                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group w-50">
                                        <label htmlFor="name">Mã Code</label>
                                        <input type="text" className="form-control" id="code"
                                            {...register('code')}
                                            value={code}
                                            onChange={(e) => setCode(e.target.value)} />
                                        {errors.code && errors.code.type === "required" && <p className="form-text text-danger">Mã Code không được để trống</p>}
                                    </div>
                                    <div className="form-group w-50">
                                        <label htmlFor="number">Số lượng</label>
                                        <input type="number" className="form-control" id="count"
                                            {...register('count')}
                                            value={count}
                                            onChange={(e) => setCount(e.target.value)} />
                                        {errors.count && errors.count.type === "required" && <p className="form-text text-danger">Số lượng không được để trống</p>}
                                    </div>
                                    <div className="form-group w-50">
                                        <label htmlFor="description">Khuyến mãi</label>
                                        <input type="text" className="form-control" id="promotion"
                                            {...register('promotion')}
                                            value={promotion}
                                            onChange={(e) => setPromotion(e.target.value)} />
                                        {errors.promotion && errors.promotion.type === "required" && <p className="form-text text-danger">Khuyến mãi không được để trống</p>}
                                    </div>
                                    <div className="form-group w-50">
                                        <label htmlFor="description">Mô tả</label>
                                        <input type="text" className="form-control" id="describe"
                                            {...register('describe')}
                                            value={describe}
                                            onChange={(e) => setDescribe(e.target.value)} />
                                        {errors.describe && errors.describe.type === "required" && <p className="form-text text-danger">Mô tả không được để trống</p>}
                                    </div>
                                    <Grid item xs={12}>
                                        <Controller
                                            name="users"
                                            control={control}
                                            render={({ field }) => (
                                                <AutoComplete
                                                    {...field}
                                                    multiple={true}
                                                    options={customers}
                                                    isOptionEqualToValue={(option, value) => value?._id === option?._id}
                                                    getOptionLabel={(option) => option?.fullname || ""}
                                                    onChange={(_event, newValue) => {
                                                        if (newValue) {
                                                            setValue('users', newValue, { shouldValidate: true });
                                                            setSelectedUsers(newValue);
                                                        }
                                                    }}
                                                    value={selectedUsers}
                                                    renderOption={(props, option, { index }) => (
                                                        <Box component="li" key={index} {...props}>
                                                            {option?.fullname || ""}
                                                        </Box>
                                                    )}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            label="Chọn khách hàng"
                                                            error={Boolean(errors.users?.message)}
                                                            helperText={errors.users?.message}
                                                        />
                                                    )}
                                                />
                                            )}
                                        />
                                    </Grid>
                                    <button type="submit" className="btn btn-primary" style={{ marginTop: '12px' }}>Cập nhật</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer text-center text-muted">
                Đã đăng ký bản quyền bởi Dimples. Thiết kế và phát triển bởi     <a href="https://www.facebook.com/Minh Anh.9920/">Minh Anh</a>.
            </footer>
        </div>
    );
}

export default UpdateCoupon;