import React, { useEffect, useState } from 'react';
import { useForm, Controller } from 'react-hook-form'
import AutoComplete from '@mui/material/Autocomplete';
import { Box, Grid, TextField } from "@mui/material";;
import queryString from 'query-string';
import userAPI from '../Api/userAPI';

import CouponAPI from '../Api/CouponAPI';

const defaultValues = {
    code: '',
    count: '',
    promotion: '',
    describe: '',
    users: []
};


function CreateCoupon(props) {

    const [showMessage, setShowMessage] = useState('')
    const [selectedUsers, setSelectedUsers] = useState([])
    const [customers, setCustomers] = useState([]);
    const [filter, setFilter] = useState({
        permission: '65389cb5d833104afc9959fe'
    })

    const { register, handleSubmit, formState: { errors }, reset } = useForm({ defaultValues });

    const { control, setValue } = useForm();

    useEffect(() => {
        const query = '?' + queryString.stringify(filter)

        const fetchAllData = async () => {
            const response = await userAPI.getAPI(query)
            setCustomers(response.users)
        }
        fetchAllData()
    }, [filter])

    const onSubmit = async (data) => {
        const tempUser = [];
        for (const user of selectedUsers) {
            tempUser.push(user._id)
        }
        const body = {
            code: data.code,
            count: data.count,
            promotion: data.promotion,
            describe: data.describe,
            users: tempUser
        }

        const response = await CouponAPI.postCoupons(body)

        setShowMessage(response.msg)

        reset({ defaultValues })
    };

    const getDepartmentOptionRef = (index, lastElementRef) => {
        return index === customers.length
            ? lastElementRef
            : null;
    };

    return (
        <div className="page-wrapper">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Tạo phiếu</h4>
                                {
                                    showMessage === "Bạn đã thêm thành công" ?
                                        (
                                            <div className="alert alert-success alert-dismissible fade show" role="alert">
                                                {showMessage}
                                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                        ) :
                                        (
                                            <p className="form-text text-danger">{showMessage}</p>
                                        )
                                }

                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group w-50">
                                        <label htmlFor="name">Mã Code</label>
                                        <input type="text" className="form-control" id="code" {...register('code', { required: true })} />
                                        {errors.code && errors.code.type === "required" && <p className="form-text text-danger">Mã Code không được để trống</p>}
                                    </div>
                                    <div className="form-group w-50">
                                        <label htmlFor="number">Số lượng</label>
                                        <input type="number" className="form-control" id="count" {...register('count', { required: true })} />
                                        {errors.count && errors.count.type === "required" && <p className="form-text text-danger">Số lượng không được để trống</p>}
                                    </div>
                                    <div className="form-group w-50">
                                        <label htmlFor="description">Khuyến mãi</label>
                                        <input type="text" className="form-control" id="promotion" {...register('promotion', { required: true })} />
                                        {errors.promotion && errors.promotion.type === "required" && <p className="form-text text-danger">Khuyến mãi không được để trống</p>}
                                    </div>
                                    <div className="form-group w-50">
                                        <label htmlFor="description">Mô tả</label>
                                        <input type="text" className="form-control" id="describe" {...register('describe', { required: true })} />
                                        {errors.describe && errors.describe.type === "required" && <p className="form-text text-danger">Mô tả không được để trống</p>}
                                    </div>
                                    <Grid item xs={12}>
                                        <Controller
                                            name="users"
                                            control={control}
                                            render={({ field }) => {
                                                return (
                                                    <AutoComplete
                                                        {...field}
                                                        multiple={true}
                                                        options={customers}
                                                        isOptionEqualToValue={(option, value) => value?._id === option?._id}
                                                        getOptionLabel={(option) => option?.fullname || ""}
                                                        onChange={(_event, newValue) => {
                                                            if (newValue) {
                                                                setValue('users', newValue, { shouldValidate: true });
                                                                setSelectedUsers(newValue)
                                                            }
                                                        }}
                                                        renderOption={(props, option, { index }) => (
                                                            <Box component="li" key={index} {...props} ref={getDepartmentOptionRef(index)}>
                                                                {option?.fullname || ""}
                                                            </Box>
                                                        )}
                                                        renderInput={(params) => (
                                                            <TextField
                                                                {...params}
                                                                label="Chọn khách hàng"
                                                                error={Boolean(errors.users?.message)}
                                                                helperText={errors.users?.message}
                                                            />
                                                        )}
                                                    />
                                                );
                                            }}
                                        />
                                    </Grid>

                                    <button type="submit" className="btn btn-primary" style={{marginTop: '12px'}}>Tạo phiếu</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer text-center text-muted">
                Đã đăng ký bản quyền bởi Dimples. Thiết kế và phát triển bởi     <a href="https://www.facebook.com/Minh Anh.9920/">Minh Anh</a>.
            </footer>
        </div>
    );
}

export default CreateCoupon;