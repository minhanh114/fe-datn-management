import React, { useState, useContext } from 'react';
import { useHistory, Redirect } from 'react-router-dom';
import isEmpty from 'validator/lib/isEmpty';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";

import userAPI from '../Api/userAPI';
import { AuthContext } from '../context/Auth'
import { useAlert } from 'react-alert'

function Login(props) {

    const { addLocal, jwt, user } = useContext(AuthContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [validationMsg, setValidationMsg] = useState('');
    const { handleSubmit } = useForm();
    let history = useHistory();
    const alert = useAlert();
    const [showPassword, setShowPassword] = useState(false);


    const validateAll = () => {
        let msg = {}
        if (isEmpty(email)) {
            msg.email = "Email không được để trống"
        }
        if (isEmpty(password)) {
            msg.password = "Password không được để trống"
        }
        setValidationMsg(msg)
        if (Object.keys(msg).length > 0) return false;
        return true;
    }

    const handleLogin = () => {
        const isValid = validateAll();
        if (!isValid) return
        login();
    }

    const login = async () => {
        const user = {
            email: email,
            password: password
        }
        const response = await userAPI.login(user)
        console.log(response);

        if (response.msg === "Đăng nhập thành công") {
            if (response.user.id_permission.permission === "Nhân Viên") {
                alert.success("Đăng nhập thành công !")
                addLocal(response.jwt, response.user)
                history.push('/dashboard');
                window.location.reload();
            }
            else if (response.user.id_permission.permission === "Admin") {
                alert.success("Đăng nhập thành công !")
                addLocal(response.jwt, response.user)
                history.push('/dashboard');
                window.location.reload();
            } else {
                setValidationMsg({ api: "Bạn không có quyền truy cập" })
            }

        } else {
            setValidationMsg({ api: response.msg })
        }
    }

    if (jwt && user && user.id_permission.permission === "Nhân Viên") {
        return <Redirect to="/dashboard" />
    } else if (jwt && user && user.id_permission.permission === "Admin") {
        return <Redirect to="/dashboard" />
    }

    return (
        <div className="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative" style={{ background: 'url(../assets/images/big/auth-bg.jpg) no-repeat center center' }}>
            <div className="auth-box row">
                <div className="col-lg-7 col-md-5 modal-bg-img" style={{
                    backgroundImage: 'url(../assets/images/big/123.png)',
                    border: '1px solid #ccc',  // Kích thước và kiểu viền
                    borderRadius: '5px',       // Độ cong của góc
                    padding: '10px'
                }}>
                </div>
                <div className="col-lg-5 col-md-7 bg-white">
                    <div className="p-3">
                        <h2 className="mt-3 text-center">Đăng nhập</h2>

                        {
                            <p className="form-text text-danger">{validationMsg.api}</p>
                        }
                        <form className="mt-4" onSubmit={handleSubmit(handleLogin)}>
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="form-group">
                                        <label className="text-dark" htmlFor="email">Email</label>
                                        <input
                                            className="form-control"
                                            type="email"
                                            name="email"
                                            placeholder="Nhập email"
                                            value={email}
                                            onChange={(e) => setEmail(e.target.value)}
                                        />
                                        <p className="form-text text-danger">{validationMsg.email}</p>
                                    </div>
                                </div>
                                <div className="col-lg-12">
                                    <div className="form-group">
                                        <label className="text-dark" htmlFor="pwd">Mật khẩu</label>
                                        <div className="input-group">
                                            <input
                                                className="form-control"
                                                name="password"
                                                type={showPassword ? "text" : "password"}
                                                value={password}
                                                onChange={(e) => setPassword(e.target.value)}
                                                placeholder="Nhập mật khẩu"
                                            />
                                            <div className="input-group-append">
                                                <span
                                                    className="input-group-text"
                                                    onClick={() => setShowPassword(!showPassword)}
                                                    style={{ cursor: 'pointer' }}
                                                >
                                                    {showPassword ? "Ẩn" : "Hiện"}
                                                </span>
                                            </div>
                                        </div>
                                        <p className="form-text text-danger">{validationMsg.password}</p>
                                    </div>
                                </div>
                                <div className="col-lg-12 text-right">
                                    <Link to="/forgetpassword"> Quên mật khẩu?</Link>
                                </div>
                                <div className="col-lg-12 text-center">
                                    <button type="submit" className="btn btn-block btn-dark">Đăng nhập</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;