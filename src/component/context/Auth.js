import React, { createContext, useState, useEffect } from 'react';
import { Redirect, useLocation, useHistory } from 'react-router-dom';
import { useAlert } from 'react-alert';

export const AuthContext = createContext();

const AuthContextProvider = (props) => {
    const [jwt, setJWT] = useState();
    const [user, setUser] = useState();
    const alert = useAlert();
    const location = useLocation();
    const history = useHistory();



    useEffect(() => {
        if (location.pathname.startsWith('/resetpassword')) {
            history.push(`/resetpassword${location.search}`);
        }

        if (location.pathname.startsWith('/forgetpassword')) {
            history.push("/forgetpassword");
        }
        localStorage.getItem('jwt') && setJWT(JSON.parse(localStorage.getItem('jwt')));
        localStorage.getItem('user') && setUser(JSON.parse(localStorage.getItem('user')));
    }, [location.pathname, history])

    const addLocal = (jwt, user) => {

        localStorage.setItem("jwt", JSON.stringify(jwt))
        localStorage.setItem("user", JSON.stringify(user))

        setJWT(jwt);
        setUser(user);

    }
    const logOut = () => {
        localStorage.removeItem("jwt")
        localStorage.removeItem("user")
        setJWT();
        setUser();
        alert.success("Đã đăng xuất !");
        <Redirect to="/login" />
    }

    return (
        <AuthContext.Provider
            value={{
                jwt,
                user,
                addLocal,
                logOut
            }}>
            {props.children}
        </AuthContext.Provider>
    );
}

export default AuthContextProvider;