import { useEffect, useMemo, useState } from "react";
import axiosClient from "../Api/axiosClient"
import Chart from "./Chart";


export default function ChartIncome() {
    const [userStats, setUserStats] = useState([]);
    const MONTHS = useMemo(
        () => [
            "Tháng 1",
            "Tháng 2",
            "Tháng 3",
            "Tháng 4",
            "Tháng 5",
            "Tháng 6",
            "Tháng 7",
            "Tháng 8",
            "Tháng 9",
            "Tháng 10",
            "Tháng 11",
            "Tháng 12",
        ],
        []
    );

    function formatCurrency(number) {
        const formatter = new Intl.NumberFormat('vi-VN', {
            style: 'currency',
            currency: 'VND'
        });
        return formatter.format(number);
    }

    useEffect(() => {
        const getStats = async () => {
            try {
                const res = await axiosClient.get('/admin/Order/income');
                res.map((item) =>
                    setUserStats((prev) => [
                        ...prev,
                        { name: MONTHS[item._id - 1], "Danh thu": formatCurrency(item.total) },
                    ])
                );

            } catch { }
        };
        getStats();
    }, [MONTHS]);

    return (
        <div>
            <Chart
                data={userStats}
                title="Danh thu hàng tháng"
                grid
                dataKey="Danh thu"
            />
        </div>
    );
}