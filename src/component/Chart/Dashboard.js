import React, { Fragment, useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { Doughnut, Line } from "react-chartjs-2";
import axiosClient from "../Api/axiosClient";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const Dashboard = () => {
    const [startDate, setStartDate] = useState(new Date((new Date()).getFullYear(), (new Date()).getMonth(), 1));
    const [endDate, setEndDate] = useState(new Date((new Date()).getFullYear(), (new Date()).getMonth() + 1, 0));
    const [incomeTotal, setIncomeTotal] = useState();
    const [products, setProducts] = useState([]);
    const [orders, setOrders] = useState([]);
    const [users, setUsers] = useState([]);
    const [vipUser, setVipUser] = useState([]);
    const [vipProduct, setVipProduct] = useState([]);


    useEffect(() => {
        const fetchAllData = async () => {
            const response = await axiosClient.get(`admin/user?permission=65389cb5d833104afc9959fe`);
            setUsers(response.users);
        };

        const fetchAllProduct = async () => {
            const response = await axiosClient.get(`admin/Product/all`);
            setProducts(response);
        };

        const fetchAllOrder = async () => {
            const response = await axiosClient.get(`admin/Order/all`);
            setOrders(response);
        };

        const getVipProduct = async () => {
            const response = await axiosClient.get(`admin/Order/product-vip`, {
                params: {
                    startDate: startDate,
                    endDate: endDate,
                },
            });
            setVipProduct(response);
        };

        const getVipUser = async () => {
            const result = await axiosClient.get('/admin/Order/customer-vip', {
                params: {
                    startDate: startDate,
                    endDate: endDate,
                },
            });
            setVipUser(result);
        };

        const getIncomeTotal = async () => {
            const result = await axiosClient.get('/admin/Order/income/all', {
                params: {
                    startDate: startDate,
                    endDate: endDate,
                },
            });
            setIncomeTotal(Number(result[0]?.total));
        };

        fetchAllData();
        fetchAllProduct();
        fetchAllOrder();
        getVipProduct();
        getVipUser();
        getIncomeTotal();
    }, []);


    const handleStartDateChange = (date) => {
        setStartDate(date);

        if (endDate && date > endDate) {
            setEndDate(date);
        }
    };

    const handleEndDateChange = (date) => {
        if (date < startDate) {
            setEndDate(startDate);
        } else {
            setEndDate(date);
        }
    };

    const handleFilterButtonClick = async () => {
        const responseProduct = await axiosClient.get(`admin/Order/product-vip`, {
            params: {
                startDate: startDate,
                endDate: endDate,
            },
        });
        setVipProduct(responseProduct);

        const responseUser = await axiosClient.get('/admin/Order/customer-vip', {
            params: {
                startDate: startDate,
                endDate: endDate,
            },
        });
        setVipUser(responseUser);

        const responseOrder = await axiosClient.get('/admin/Order/income/all', {
            params: {
                startDate: startDate,
                endDate: endDate,
            },
        });
        setIncomeTotal(Number(responseOrder[0]?.total));
    };

    let outOfStock = 0;
    products.forEach(product => {
        if (product.number <= 0 || !product.number) {
            outOfStock += 1;
        }
    })
    // status order
    let da_dat_hang = 0;
    let dang_van_chuyen = 0;
    let da_giao_hang = 0;
    orders &&
        orders.forEach((order) => {
            if (order.status === "1") {
                da_dat_hang += 1;
            }
            if (order.status === "3") {
                dang_van_chuyen += 1;
            }
            if (order.status === "4") {
                da_giao_hang += 1;
            }
        });

    let totalAmountall = 0;
    orders &&
        orders.forEach((product) => {
            totalAmountall += 1;
        });

    // Chart Line tính tổng doanh thu
    const lineState = {
        labels: ["Số tiền ban đầu", "Tổng danh thu hiện tại"],
        datasets: [
            {
                label: "TỔNG DANH THU",
                backgroundColor: ["blue"],
                hoverBackgroundColor: ["rgb(197, 72, 49)"],
                data: [0, incomeTotal],
            },
        ],
    };
    // Doughnut tính số lượng hàng còn và hết hàng
    const doughnutState = {
        labels: ["Hết hàng", "Còn hàng"],
        datasets: [
            {
                backgroundColor: ["#00A6B4", "#6800B4"],
                hoverBackgroundColor: ["#FF1493", "#FFD700"],
                data: [outOfStock, products.length - outOfStock],
            },
        ],
    };

    // Doughnut thống kê trạng thái đơn hàng
    const doughnutStateOrder = {
        labels: ["Đã đặt hàng", "Đang vận chuyển", "Đã giao hàng"],
        datasets: [
            {
                backgroundColor: ["#00A6B4", "#6800B4", "#FF7F50"],
                hoverBackgroundColor: ["#FF1493", "#00FA9A", "#FFD700"],
                data: [da_dat_hang, dang_van_chuyen, da_giao_hang],
            },
        ],
    };

    //chuyển đổi tiền tệ
    function formatCurrency(number) {
        const formatter = new Intl.NumberFormat('vi-VN', {
            style: 'currency',
            currency: 'VND'
        });
        return formatter.format(number);
    }

    const CustomerRankingTable = ({ data }) => {
        return (
            <div className="customer-ranking-table">
                <table>
                    <thead>
                        <tr>
                            <th>Tên khách hàng</th>
                            <th>Tiền mua sắm</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((customer, index) => (
                            <tr key={index}>
                                <td>{customer.fullname}</td>
                                <td>{formatCurrency(customer.total)}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    };

    const ProductRankingTable = ({ data }) => {
        return (
            <div className="customer-ranking-table">
                <table>
                    <thead>
                        <tr>
                            <th>Tên sản phẩm</th>
                            <th>Đã bán</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((product, index) => (
                            <tr key={index}>
                                <td>{product.name}</td>
                                <td>{product.total + ' chiếc'}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    };

    const filterButtonStyle = {
        backgroundColor: '#5f76e8',
        color: '#ffffff',
        marginLeft: '10px',
        padding: '5px 10px',
        fontSize: '14px',
        border: 'none',
        cursor: 'pointer',
        transition: 'background-color 0.3s ease',
    };

    const filterButtonHoverStyle = {
        backgroundColor: '#5f76e8',
    };


    return (
        <div className="page-wrapper">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Thống kê
                                    <div className="d-flex justify-content-center mt-3">
                                        <div className="mr-2">
                                            Từ: <DatePicker selected={startDate} onChange={handleStartDateChange} dateFormat="dd/MM/yyyy" />
                                        </div>
                                        <div>
                                            Đến: <DatePicker selected={endDate} onChange={handleEndDateChange} dateFormat="dd/MM/yyyy" />
                                        </div>
                                        <button
                                            style={{ ...filterButtonStyle, ...(filterButtonHoverStyle || {}) }}
                                            onClick={handleFilterButtonClick}
                                        >
                                            Lọc
                                        </button>
                                    </div>
                                </h4>
                                <Fragment>
                                    <div className="row pr-4">
                                        <div className="col-xl-3 col-sm-6 mb-3">
                                            <div className="card text-black bg-light o-hidden h-100">
                                                <div className="card-body">
                                                    <div className="text-center card-font-size">Xếp hạng khách hàng VIP<br />
                                                        <div className="overflow-auto max-height-10">
                                                            <CustomerRankingTable data={vipUser} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <Link className="card-footer text-black clearfix small z-1" to="/Order">
                                                    <span className="float-left">Xem chi tiết</span>
                                                    <span className="float-right">
                                                        <i className="fa fa-angle-right"></i>
                                                    </span>
                                                </Link>
                                            </div>
                                        </div>
                                        <div className="col-xl-3 col-sm-6 mb-3">
                                            <div className="card text-black bg-light o-hidden h-100">
                                                <div className="card-body">
                                                    <div className="text-center card-font-size">Sản phẩm bán chạy<br />
                                                        <div className="overflow-auto max-height-10">
                                                            <ProductRankingTable data={vipProduct} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <Link className="card-footer text-black clearfix small z-1" to="/Product">
                                                    <span className="float-left">Xem chi tiết</span>
                                                    <span className="float-right">
                                                        <i className="fa fa-angle-right"></i>
                                                    </span>
                                                </Link>
                                            </div>
                                        </div>
                                        <div className="col-xl-3 col-sm-6 mb-3">
                                            <div className="card text-black bg-light o-hidden h-100">
                                                <div className="card-body">
                                                    <div className="text-center card-font-size">Tổng sản phẩm<br /> <b>{products && products.length}</b></div>
                                                </div>
                                                <Link className="card-footer text-black clearfix small z-1" to="/Product">
                                                    <span className="float-left">Xem chi tiết</span>
                                                    <span className="float-right">
                                                        <i className="fa fa-angle-right"></i>
                                                    </span>
                                                </Link>
                                            </div>
                                        </div>

                                        <div className="col-xl-3 col-sm-6 mb-3">
                                            <div className="card text-black bg-light o-hidden h-100">
                                                <div className="card-body">
                                                    <div className="text-center card-font-size">Tổng người dùng<br /> <b>{users && users.length}</b></div>
                                                </div>
                                                <Link className="card-footer text-black clearfix small z-1" to="/Customer">
                                                    <span className="float-left">Xem chi tiết</span>
                                                    <span className="float-right">
                                                        <i className="fa fa-angle-right"></i>
                                                    </span>
                                                </Link>
                                            </div>
                                        </div>

                                        <div className="col-xl-3 col-sm-6 mb-3">
                                            <div className="card text-white bg-light o-hidden h-100">
                                                <div className="card-body">
                                                    <h6 className='text-dark'>Tình trạng số lượng hàng</h6>
                                                    <div className="doughnutChart">
                                                        <Doughnut data={doughnutState} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-xl-3 col-sm-6 mb-3">
                                            <div className="card text-white bg-light o-hidden h-100">
                                                <div className="card-body">
                                                    <h6 className='text-dark'>Tình trạng đơn hàng</h6>
                                                    <div className="doughnutChart">
                                                        <Doughnut data={doughnutStateOrder} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-xl-6 col-sm-12 mb-3">
                                            <div className="card text-white bg-light o-hidden h-100">
                                                <div className="card-body">
                                                    {/* <h6 className='text-dark'>Tổng doanh thu </h6> */}
                                                    {/* Line chart */}
                                                    <div className="lineChart">
                                                        <Line
                                                            data={lineState}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </Fragment>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer text-center text-muted">
                Đã đăng ký bản quyền bởi Dimples. Thiết kế và phát triển bởi <a
                    href="https://www.facebook.com/Minh Anh.9920/">Minh Anh</a>.
            </footer>
        </div>

    )
}

export default Dashboard
