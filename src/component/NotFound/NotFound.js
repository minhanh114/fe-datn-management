import React from 'react';
import PropTypes from 'prop-types';

NotFound.propTypes = {

};

function NotFound(props) {
    return (
        <div className="page-wrapper">
            <div className="container-fluid">
                <h1 style={{ fontWeight: 'bolder', color: "black" }}>Error 404!</h1>
            </div>
            <footer className="footer text-center text-muted">
                Đã đăng ký bản quyền bởi Dimples. Thiết kế và phát triển bởi     <a
                    href="https://www.facebook.com/Minh Anh.9920/">Minh Anh</a>.
        </footer>
        </div >
    );
}

export default NotFound;