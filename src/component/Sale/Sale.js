import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import queryString from 'query-string'

import Pagination from '../Shared/Pagination'
import Search from '../Shared/Search'
import SaleAPI from '../Api/SaleAPI';

function Sale(props) {
    const [filter, setFilter] = useState({
        page: '1',
        limit: '4',
        search: '',
        status: true
    })

    const [sale, setSale] = useState([])
    const [totalPage, setTotalPage] = useState()

    useEffect(() => {
        const query = '?' + queryString.stringify(filter)

        const fetchAllData = async () => {
            const ct = await SaleAPI.getAll(query)
            setTotalPage(ct.totalPage)
            setSale(ct.sale)
        }

        fetchAllData()
    }, [filter])

    const onPageChange = (value) => {
        setFilter({
            ...filter,
            page: value
        })
    }

    const handlerSearch = (value) => {
        setFilter({
            ...filter,
            page: '1',
            search: value
        })
    }


    return (
        <div className="page-wrapper">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Khuyến mãi</h4>
                                <Search handlerSearch={handlerSearch} />

                                <Link to="/sale/create" className="btn btn-primary my-3">Tạo mới</Link>


                                <div className="table-responsive">
                                    <table className="table table-striped table-bordered no-wrap">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Phần trăm</th>
                                                <th>Mô tả</th>
                                                <th>Thời gian bắt đầu</th>
                                                <th>Thời gian kết thúc</th>
                                                <th>Trạng thái</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            {
                                                sale && sale.map((value, index) => (
                                                    <tr key={index}>
                                                        <td className="name">{value._id}</td>
                                                        <td className="name">{value.promotion + '%'}</td>
                                                        <td className="name">{value.describe}</td>
                                                        <td className="name">{new Date(value.start).toLocaleString()}</td>
                                                        <td className="name">{new Date(value.end).toLocaleString()}</td>
                                                        <td className="name">{value.status ? "Hoạt động" : "Không hoạt động"}</td>
                                                        <td>
                                                            <div className="d-flex">
                                                                <Link to={"/sale/" + value._id} className="btn btn-success mr-1">Cập nhật</Link>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                    </table>
                                    <Pagination filter={filter} onPageChange={onPageChange} totalPage={totalPage} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer text-center text-muted">
                Đã đăng ký bản quyền bởi Dimples. Thiết kế và phát triển bởi     <a
                    href="https://www.facebook.com/Minh Anh.9920/">Minh Anh</a>.
            </footer>
        </div>
    );
}

export default Sale;