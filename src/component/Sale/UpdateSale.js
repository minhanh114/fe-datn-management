import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form'
import "react-datepicker/dist/react-datepicker.css";
import productAPI from '../Api/productAPI';
import SaleAPI from '../Api/SaleAPI';
import { useParams } from 'react-router';

const defaultValues = {
    promotion: '',
    describe: '',
};

function UpdateSale(props) {

    const { id } = useParams()

    const [showMessage, setShowMessage] = useState('')

    const { register, handleSubmit, formState: { errors }, reset } = useForm({ defaultValues });
    const onSubmit = async (data) => {

        const body = {
            promotion: promotion,
            describe: describe,
            status: status,
            id_product: selectProduct
        }
        if (!selectProduct) {
            delete body.id_product
        }
        const response = await SaleAPI.updateSale(id, body)
        setSelectedProduct(response.data?.id_product?.name_product)
        setShowMessage(response.msg)

    };

    const [promotion, setPromotion] = useState('')
    const [describe, setDescribe] = useState('')
    const [status, setStatus] = useState('')
    let [selectProduct, setSelectProduct] = useState('')
    const [selectedProduct, setSelectedProduct] = useState('')

    const [product, setProduct] = useState([])

    useEffect(() => {

        const fetchData = async () => {

            const response = await productAPI.getAll()
            setProduct(response)

            const resDetail = await SaleAPI.detailSale(id)

            setStatus(resDetail.status)
            setPromotion(resDetail.promotion)
            setDescribe(resDetail.describe)
            setSelectedProduct(resDetail?.id_product?.name_product || 'trống')

        }

        fetchData()

    }, [])

    return (
        <div className="page-wrapper">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Cập nhật khuyến mãi</h4>
                                {
                                    showMessage === "Bạn đã cập nhật thành công" ?
                                        (
                                            <div className="alert alert-success alert-dismissible fade show" role="alert">
                                                {showMessage}
                                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                        ) :
                                        (
                                            <p className="form-text text-danger">{showMessage}</p>
                                        )
                                }


                                <form onSubmit={handleSubmit(onSubmit)}>

                                    <div className="form-group w-50">
                                        <label htmlFor="name">Phần trăm</label>
                                        <input type="number" className="form-control" id="promotion"
                                            {...register('promotion')}
                                            value={promotion}
                                            onChange={(e) => setPromotion(e.target.value)} />
                                        {errors.promotion && errors.promotion.type === "required" && <p className="form-text text-danger">Khuyến Mãi không được để trống</p>}
                                    </div>
                                    <div className="form-group w-50">
                                        <label htmlFor="description">Mô tả</label>
                                        <input type="text" className="form-control" id="describe"
                                            {...register('describe')}
                                            value={describe}
                                            onChange={(e) => setDescribe(e.target.value)} />
                                        {errors.describe && errors.describe.type === "required" && <p className="form-text text-danger">Mô tả không được để trống</p>}
                                    </div>
                                    <div className="form-group w-50">
                                        <label htmlFor="description">Đổi trạng thái</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="true" onClick={(e) => setStatus(e.target.value)} />
                                            <label class="form-check-label" for="gridRadios1">
                                                Hoạt Động
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="false" onClick={(e) => setStatus(e.target.value)} />
                                            <label class="form-check-label" for="gridRadios2">
                                                Ngưng Hoạt Động
                                            </label>
                                        </div>
                                    </div>

                                    <div className="form-group w-25">
                                        <label htmlFor="description">Danh sách sản phẩm có thể áp dụng</label>
                                        <select className="form-control" value={selectProduct} onChange={(e) => setSelectProduct(e.target.value)}>
                                            <option value="" disabled selected>Chọn sản phẩm</option>
                                            {
                                                product && product.map(value => (
                                                    <option value={value._id} key={value._id}>{value.name_product}</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                    <p>Sản phẩm đang được áp dụng: {selectedProduct}</p>

                                    <button type="submit" className="btn btn-primary">Cập nhật</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer text-center text-muted">
                Đã đăng ký bản quyền bởi Dimples. Thiết kế và phát triển bởi <a href="https://www.facebook.com/Minh Anh.9920/">Minh Anh</a>.
            </footer>
        </div>
    );
}

export default UpdateSale;